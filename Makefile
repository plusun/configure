all:
	@echo "What you want?"
simple:
	(cd emacs; make)
	(cd zsh; make)
	(cd tmux; make)
	cp Xresources ~/.Xdefaults
	cp gitconfig ~/.gitconfig
	mkdir -p ~/.config/awesome
	cp -r awesome/* ~/.config/awesome
noHhkb:
	(cd keymap; make)
dorm: simple
	(cd ssh; make)
terminal:
	(cd zsh; make)
	(cd tmux; make)
	cp Xresources ~/.Xdefaults

